{ pkgs ? import <nixpkgs> { } }:
with pkgs; stdenv.mkDerivation {
  name = "create-volts-curseforge";
  src = ./src;

  nativeBuildInputs = [
    packwiz
    p7zip
  ];

  buildPhase = ''
    src=$(pwd)
    mkdir -p "$out"
    cd "$out"
    packwiz curseforge export --pack-file "$src/pack.toml"
    # Workaround for https://github.com/packwiz/packwiz/issues/263
    file=$(find . -name '*.zip')
    7z e "$file" manifest.json -so | grep --invert-match '"projectID": 0,' > manifest.json 
    7z a "$file" manifest.json
  '';

  installPhase = "";
}