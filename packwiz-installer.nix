{ pkgs ? import <nixpkgs> { } }:
with pkgs; stdenv.mkDerivation {
  name = "create-volts-packwiz";
  src = ./src;

  nativeBuildInputs = [
    packwiz
  ];

  buildPhase = ''
    packwiz refresh --build
  '';

  installPhase = ''
    mkdir -p "$out"
    cp -r * "$out"
  '';
}