// priority: 0

const IE = (id) => `immersiveengineering:${id}`

/**
 * Execute `callback` for every recipe that matches `filter` and replace the original one with the one that
 * `callback` returned
 * @param {Internal.RecipesEventJS} e
 * @param {any} filter
 * @param {function(any):any} callback
 */
function map_recipes(e, filter, callback) {
    e.forEachRecipe(filter, (recipeJSON) => {
        e
            .custom(callback(JSON.parse(recipeJSON.json)))
            .id(recipeJSON.getOrCreateId())
    })
}

/**
 * Create Create-like recipe
 * @param {Internal.RecipesEventJS} e
 * @param {string} type
 * @param {Array<Object>} ingredients
 * @param {Array<Object>} results
 * @param {string} [heatRequirement]
 * @param {number} [processingTime]
 */
function create_recipe(
    e,
    type,
    ingredients,
    results,
    heatRequirement,
    processingTime
) {
    const recipe = {
        type: type,
        ingredients: ingredients,
        results: results,
    }
    if (heatRequirement) recipe["heatRequirement"] = heatRequirement
    if (processingTime) recipe["processingTime"] = processingTime
    e.custom(recipe)
}

/**
 * Create Create compacting recipe.
 * @param {Internal.RecipesEventJS} e
 * @param {Array<Object>} ingredients
 * @param {Array<Object>} results
 * @param {string} [heatRequirement]
 */
function compact(e, ingredients, results, heatRequirement) {
    create_recipe(
        e,
        "create:compacting",
        ingredients,
        results,
        heatRequirement
    )
}

/**
 * Create Create filling recipe.
 * @param {Internal.RecipesEventJS} e
 * @param {Array<Object>} ingredients
 * @param {Array<Object>} results
 */
function spout(e, ingredients, results) {
    create_recipe(e, "create:filling", ingredients, results)
}

/**
 * Create Create filling recipe.
 * @param {Internal.RecipesEventJS} e
 * @param {Array<Object>} ingredients
 * @param {Array<Object>} results
 * @param {number} processingTime
 */
function create_crush(e, ingredients, results, processingTime) {
    const main_result = results[0]
    const secondary_chance = main_result.count % 1
    if (secondary_chance !== 0) {
        results.splice(1, 0, {
            item: main_result.item,
            chance: secondary_chance,
        })
        results[0] = {
            item: main_result.item,
            count: Math.floor(main_result.count),
        }
    }
    create_recipe(
        e,
        "create:crushing",
        ingredients,
        results,
        "",
        processingTime
    )
}

/**
 * Create Immersive Engineering Crusher recipe.
 *
 * `results` are specified like this: `[[1.5, "minecraft:dirt"], [0.25, "minecraft:diamond"]]`. The first item in the
 * `results` array is the main result. It can have any decimal number. Secondary results can have numbers x where 0 < x <= 1.
 * @param {Internal.RecipesEventJS} e
 * @param {string} ingredient
 * @param {Array<Array>} results
 * @param {number} energy
 */
function ie_crush(e, ingredient, results, energy) {
    const recipe = {
        type: IE("crusher"),
        energy: energy,
        input: { item: ingredient },
        result: {},
        secondaries: [],
    }

    const main_result = {
        item: results[0][1],
        count: results[0][0],
        count_floor: Math.floor(results[0][0]),
    }
    if (main_result.count_floor === 1) {
        recipe.result = { item: main_result.item }
    } else {
        recipe.result = {
            base_ingredient: {
                item: main_result.item,
            },
            count: main_result.count_floor,
        }
    }
    let secondary_chance = main_result.count % 1
    if (secondary_chance !== 0) {
        recipe.secondaries.push({
            chance: secondary_chance,
            output: { item: main_result.item },
        })
    }
    if (results.length > 1) {
        results.slice(1).forEach((result) => {
            recipe.secondaries.push({
                chance: result[0],
                output: {
                    item: result[1],
                },
            })
        })
    }
    e.custom(recipe)
}

ServerEvents.recipes((e) => {
    // This is configured in kubejs/config/hide_and_remove.json
    // See also kubejs/startup_scripts/hide_and_remove.js
    global.hide_and_remove.items.forEach((id) => e.remove({ output: id }))
    global.hide_and_remove.fluids.forEach((id) => e.remove({ output: id }))

    // Disable making crushed ore with the IE hammer
    e.remove({ type: IE("hammer_crushing") })

    // Force player to use the Industrial Hemp recipe from CC&A for Create Sails
    e.remove({ id: "create:crafting/kinetics/white_sail" })

    // IE Bottling Machine recipes ported to Create
    e.remove({ type: IE("bottling_machine") })
    compact(
        e,
        [{ fluid: IE("phenolic_resin"), amount: 1000 }],
        [{ item: IE("duroplast") }]
    )
    compact(
        e,
        [
            { item: IE("mold_plate") },
            { fluid: IE("phenolic_resin"), amount: 250 },
        ],
        [{ item: IE("mold_plate") }, { item: IE("plate_duroplast") }]
    )
    compact(
        e,
        [
            { tag: "forge:fabric_hemp" },
            { fluidTag: "forge:plantoil", amount: 125 },
        ],
        [{ item: IE("ersatz_leather") }]
    )
    compact(
        e,
        [
            { item: IE("mold_bullet_casing") },
            { tag: "forge:nuggets/copper" },
            { fluid: IE("phenolic_resin"), amount: 100 },
        ],
        [{ item: IE("mold_bullet_casing") }, { item: IE("empty_shell") }]
    )
    compact(
        e,
        [
            { item: IE("mold_gear") },
            { tag: "forge:plates/aluminum" },
            { tag: "forge:fabric_hemp" },
        ],
        [{ item: IE("grindingdisk") }],
        "heated"
    )
    spout(
        e,
        [
            { item: "minecraft:copper_block" },
            { fluid: IE("redstone_acid"), amount: 125 },
        ],
        [{ item: "minecraft:exposed_copper" }]
    )
    spout(
        e,
        [
            { item: "minecraft:sponge" },
            { fluidTag: "minecraft:water", amount: 1000 },
        ],
        [{ item: "minecraft:wet_sponge" }]
    )

    e.remove({ type: IE("sawmill") })
    // Sawdust
    map_recipes(
        e,
        { type: "create:cutting", input: "#minecraft:logs" },
        (recipe) => {
            recipe.results.push({ item: IE("dust_wood") })
            return recipe
        }
    )

    ie_crush(
        e,
        "minecraft:diamond",
        [[1.33, "createaddition:diamond_grit"]],
        3000
    )

    // IE Metal Press
    e.remove({ type: IE("metal_press") })
    compact(
        e,
        [
            { item: IE("mold_bullet_casing") },
            { item: "minecraft:copper_ingot" },
        ],
        [{ item: IE("mold_bullet_casing") }, { item: IE("empty_casing") }]
    )
    compact(
        e,
        [
            { tag: "forge:ingots/hop_graphite" },
            { tag: "forge:ingots/hop_graphite" },
            { tag: "forge:ingots/hop_graphite" },
            { tag: "forge:ingots/hop_graphite" },
        ],
        [{ item: IE("graphite_electrode"), nbt: "{graphDmg:48000}" }]
    )

    // Balance Biodiesel production
    e.remove({ id: "createaddition:compacting/seed_oil" })
    e.remove({ id: "createaddition:mixing/bioethanol" })

    // Gate steel behind crushing wheels
    e.replaceInput(
        { id: IE("crafting/blastbrick") },
        "minecraft:nether_brick",
        "create:cinder_flour"
    )

    // Unify vacuum & electron tubes
    e.remove({ id: IE("blueprint/electron_tube") })
    e.remove({ id: "create:crafting/materials/electron_tube" })
    e.custom({
        type: IE("blueprint"),
        category: "components",
        inputs: [
            {
                tag: "forge:glass",
            },
            {
                tag: "forge:plates/nickel",
            },
            {
                tag: "forge:wires/copper",
            },
            {
                item: "create:polished_rose_quartz",
            },
        ],
        result: {
            count: 3,
            item: "create:electron_tube",
        },
    })

    // Remove IE Wirecutter wire recipes
    const wires = ["steel", "electrum", "copper", "lead", "aluminum"]
    wires.forEach((wire) => e.remove({ id: IE(`crafting/wire_${wire}`) }))

    // Balance Crushing Wheels vs IE Crusher
    map_recipes(e, { id: "create:crushing/blaze_rod" }, (recipe) => {
        recipe.results.pop()
        return recipe
    })

    e.shaped("create:mechanical_drill", ["D", "A"], {
        D: IE("drillhead_steel"),
        A: "create:andesite_casing",
    }).id("create:crafting/kinetics/mechanical_drill")

    e.shaped("create:mechanical_saw", ["S", "A"], {
        S: IE("sawblade"),
        A: "create:andesite_casing",
    }).id("create:crafting/kinetics/mechanical_saw")

    e.replaceInput(
        [
            { id: "create:crafting/kinetics/mechanical_harvester" },
            { id: "create:crafting/kinetics/propeller" },
        ],
        "#forge:plates/iron",
        "#forge:plates/steel"
    )

    /*
     * Ore processing
     */
    function create_byproducts(count, id, variant) {
        let byproduct_id = "minecraft:cobblestone"

        if (variant === "deepslate") {
            byproduct_id = "minecraft:cobbled_deepslate"
        } else if (variant === "nether") {
            byproduct_id = "minecraft:netherrack"
        }

        return [
            [count, id],
            [0.75, "create:experience_nugget"],
            [0.12, byproduct_id],
        ]
    }

    const ie_metals = ["aluminum", "lead", "silver", "nickel", "uranium"]
    ie_metals.forEach((metal) => {
        e.remove({ id: `create:crushing/${metal}_ore` })
        create_crush(
            e,
            [{ item: IE(`ore_${metal}`) }],
            [
                { count: 1.75, item: `create:crushed_raw_${metal}` },
                { chance: 0.75, item: "create:experience_nugget" },
                { chance: 0.12, item: "minecraft:cobblestone" },
            ],
            250
        )
        create_crush(
            e,
            [{ item: IE(`deepslate_ore_${metal}`) }],
            [
                { count: 2.25, item: `create:crushed_${metal}` },
                { chance: 0.75, item: "create:experience_nugget" },
                { chance: 0.12, item: "minecraft:cobbled_deepslate" },
            ],
            350
        )
    })

    const crusher_metals = {
        immersiveengineering: ie_metals,
        minecraft: ["iron", "gold"],
        create: ["zinc"],
    }
    for (const namespace in crusher_metals) {
        crusher_metals[namespace].forEach((metal) => {
            e.remove({
                id: IE(`crusher/ore_${metal}`),
            })

            let ore_id
            let deepslate_ore_id
            if (namespace === "immersiveengineering") {
                ore_id = `${namespace}:ore_${metal}`
                deepslate_ore_id = `${namespace}:deepslate_ore_${metal}`
            } else {
                ore_id = `${namespace}:${metal}_ore`
                deepslate_ore_id = `${namespace}:deepslate_${metal}_ore`
            }
            ie_crush(
                e,
                ore_id,
                create_byproducts(2, `create:crushed_raw_${metal}`),
                6000
            )
            ie_crush(
                e,
                deepslate_ore_id,
                create_byproducts(
                    2.5,
                    `create:crushed_raw_${metal}`,
                    "deepslate"
                ),
                6000
            )
        })
    }

    // Copper outputs more
    e.remove({ id: IE("crusher/ore_copper") })
    ie_crush(
        e,
        "minecraft:copper_ore",
        create_byproducts(7, "create:crushed_raw_copper"),
        6000
    )
    ie_crush(
        e,
        "minecraft:deepslate_copper_ore",
        create_byproducts(9, "create:crushed_raw_copper", "deepslate"),
        6000
    )

    const gems = ["diamond", "emerald"]
    gems.forEach((gem) => {
        e.remove({ id: IE(`crusher/ore_${gem}`) })
        ie_crush(
            e,
            `minecraft:${gem}_ore`,
            create_byproducts(2, `minecraft:${gem}`),
            6000
        )
        ie_crush(
            e,
            `minecraft:deepslate_${gem}_ore`,
            create_byproducts(2.5, `minecraft:${gem}`, "deepslate"),
            6000
        )
    })

    // Coal
    e.remove({ id: IE("crusher/ore_coal") })
    ie_crush(
        e,
        "minecraft:coal_ore",
        create_byproducts(2, "minecraft:coal").concat([
            [0.25, IE("dust_sulfur")],
        ]),
        6000
    )
    ie_crush(
        e,
        "minecraft:deepslate_coal_ore",
        create_byproducts(2, "minecraft:coal", "deepslate").concat([
            [0.25, IE("dust_sulfur")],
        ]),
        6000
    )

    // Nether Gold Ore
    e.remove({ id: IE("crusher/nether_gold") })
    ie_crush(
        e,
        "minecraft:nether_gold_ore",
        create_byproducts(20, "minecraft:gold_nugget", "nether"),
        6000
    )

    // Remove gunpowder as a byproduct of zinc splashing
    map_recipes(e, { id: "create:splashing/crushed_zinc_ore" }, (recipe) => {
        recipe.results.pop()
        return recipe
    })

    // Amethyst
    ie_crush(
        e,
        "minecraft:amethyst_cluster",
        [[8.5, "minecraft:amethyst_shard"]],
        3000
    )

    // Lapis
    e.remove({ id: IE("crusher/ore_lapis") })
    ie_crush(
        e,
        "minecraft:lapis_ore",
        create_byproducts(11.5, "minecraft:lapis_lazuli"),
        6000
    )
    ie_crush(
        e,
        "minecraft:deepslate_lapis_ore",
        create_byproducts(13.5, "minecraft:lapis_lazuli", "deepslate"),
        6000
    )

    // Redstone
    e.remove({ id: IE("crusher/ore_redstone") })
    ie_crush(
        e,
        "minecraft:redstone_ore",
        create_byproducts(7, "minecraft:redstone"),
        6000
    )
    ie_crush(
        e,
        "minecraft:deepslate_redstone_ore",
        create_byproducts(8, "minecraft:redstone"),
        6000
    )

    // Buff Arc Furnace to produce 1.25 ingots per crushed ore
    let all_metals = []
    for (const namespace in crusher_metals) {
        all_metals = all_metals.concat(crusher_metals[namespace])
    }
    all_metals.push("copper")
    all_metals.forEach((metal) => {
        e.remove({ id: IE(`crusher/ingot_${metal}`) })
        map_recipes(e, { id: IE(`arcfurnace/dust_${metal}`) }, (recipe) => {
            if (!recipe.secondaries) {
                recipe.secondaries = []
            }
            recipe.secondaries.push({
                output: { tag: `forge:ingots/${metal}` },
                chance: 0.25,
            })
            return recipe
        })
    })
})
