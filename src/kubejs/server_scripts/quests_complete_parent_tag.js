// priority: 0

/*
 * Handling for the `complete_parent` quest tag
 *
 * If the tagged task is completed, complete the entire quest
 */
FTBQuestsEvents.completed("#complete_parent", (e) => {
    e.data.complete(
        e.object.questFile.getQuest(e.object.parentID).codeString
    )
})
