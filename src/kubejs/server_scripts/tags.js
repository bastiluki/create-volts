// priority: 0

const my_tags = [
    ["forge:dusts/iron", "create:crushed_raw_iron"],
    ["forge:dusts/gold", "create:crushed_raw_gold"],
    ["forge:dusts/copper", "create:crushed_raw_copper"],
    ["forge:dusts/zinc", "create:crushed_raw_zinc"],
    ["forge:dusts/silver", "create:crushed_raw_silver"],
    ["forge:dusts/lead", "create:crushed_raw_lead"],
    ["forge:dusts/aluminum", "create:crushed_raw_aluminum"],
    ["forge:dusts/uranium", "create:crushed_raw_uranium"],
    ["forge:dusts/nickel", "create:crushed_raw_nickel"],
    [IE("circuits/logic"), "create:electron_tube"],
    ["createvolts:electron_tube", "create:electron_tube", IE("electron_tube")],
]
ServerEvents.tags("item", (e) => {
    my_tags.forEach((entry) => {
        const tag = e.get(entry[0])
        entry.slice(1).forEach((item) => tag.add(item))
    })

    // e.removeAllTagsFrom(global.hide_and_remove.items)
})

ServerEvents.tags("fluid", (e) => {
    // This is configured in kubejs/config/hide_and_remove.json
    // See also kubejs/startup_scripts/hide_and_remove.js
    e.removeAllTagsFrom(global.hide_and_remove.fluids)
})
