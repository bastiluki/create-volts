{
	inputs = {
		flake-utils.url = "github:numtide/flake-utils";
	};

	outputs = { self, nixpkgs, flake-utils }:
		flake-utils.lib.eachDefaultSystem (system:
			let 
				pkgs = nixpkgs.legacyPackages.${system};
			in rec {
				packages.packwiz-installer = import ./packwiz-installer.nix { pkgs = pkgs; };
				packages.packwiz-export = import ./packwiz-export.nix { pkgs = pkgs; };
				packages.default = packages.packwiz-export;

				devShell = pkgs.mkShell {
					buildInputs = [ pkgs.packwiz ]; 
					# This function lets us use packwiz anywhere while referring to the pack.toml file in src.
					# Not entirely foolproof, probably. The --pack-file option is currently bugged.
					# https://github.com/packwiz/packwiz/issues/264#issuecomment-1838656268
					shellHook = ''
						dir=$(git rev-parse --show-toplevel)
						packwiz () {
							pushd "$dir/src/"
							command packwiz $@
							popd 
						}
					'';
				};
			}
		);
}
